/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.chanisata.lab3_testcase;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class OXGameJUnitTest {

    public OXGameJUnitTest() {
    }

    @BeforeAll
    public static void setUpClass() {
    }

    @AfterAll
    public static void tearDownClass() {
    }

    @BeforeEach
    public void setUp() {
    }

    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    //ยังไม่เริ่มเล่น O
    @Test
    public void testCheckWinNoPlayRowBY_O_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

    // O ฝ่ายชนะ แถวที่ 1
    @Test
    public void testCheckWinRow1BY_O_output_true() {
        String[][] table = {{"O", "O", "O"}, {"X", "X", "-"}, {"-", "-", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // O ฝ่ายชนะ แถวที่ 2
    public void testCheckWinRow2BY_O_output_true() {
        String[][] table = {{"X", "-", "X"}, {"O", "O", "O"}, {"-", "X", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // O ฝ่ายชนะ แถวที่ 3
    public void testCheckWinRow3BY_O_output_true() {
        String[][] table = {{"-", "-", "X"}, {"-", "X", "X"}, {"O", "O", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    //ยังไม่เริ่มเล่น X
    @Test
    public void testCheckWinNoPlayRowBY_X_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

    // X ฝ่ายชนะ แถวที่ 1
    @Test
    public void testCheckWinRow1BY_X_output_true() {
        String[][] table = {{"X", "X", "X"}, {"-", "O", "O"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // X ฝ่ายชนะ แถวที่ 2
    @Test
    public void testCheckWinRow2BY_X_output_true() {
        String[][] table = {{"-", "0", "-"}, {"X", "X", "X"}, {"-", "0", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // X ฝ่ายชนะ แถวที่ 3
    @Test
    public void testCheckWinRow3BY_X_output_true() {
        String[][] table = {{"-", "-", "O"}, {"-", "O", "-"}, {"X", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinNoPlayColBY_O_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

    // O ฝ่ายชนะ หลักที่ 1
    @Test
    public void testCheckWinNoPlayCol1BY_O_output_true() {
        String[][] table = {{"O", "-", "-"}, {"O", "X", "X"}, {"O", "X", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // O ฝ่ายชนะ หลักที่ 2
    @Test
    public void testCheckWinNoPlayCol2BY_O_output_true() {
        String[][] table = {{"X", "O", "O"}, {"X", "O", "X"}, {"-", "O", "X"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // O ฝ่ายชนะ หลักที่ 3
    @Test
    public void testCheckWinNoPlayCol3BY_O_output_true() {
        String[][] table = {{"X", "O", "O"}, {"X", "X", "O"}, {"-", "X", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    @Test
    public void testCheckWinNoPlayColBY_X_output_false() {
        String[][] table = {{"-", "-", "-"}, {"-", "-", "-"}, {"-", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
    }

    // X ฝ่ายชนะ หลักที่ 1
    @Test
    public void testCheckWinNoPlayCol1BY_X_output_true() {
        String[][] table = {{"X", "-", "-"}, {"X", "O", "-"}, {"X", "-", "O"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // X ฝ่ายชนะ หลักที่ 2
    @Test
    public void testCheckWinNoPlayCol2BY_X_output_true() {
        String[][] table = {{"O", "X", "O"}, {"-", "X", "O"}, {"-", "X", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // X ฝ่ายชนะ หลักที่ 3
    @Test
    public void testCheckWinNoPlayCol3BY_X_output_true() {
        String[][] table = {{"O", "-", "X"}, {"O", "X", "X"}, {"-", "O", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // O ฝ่ายชนะ แนวทแยง 1
    @Test
    public void testCheckwinX1BY_O_output_true() {
        String[][] table = {{"O", "-", "X"}, {"-", "O", "X"}, {"-", "X", "O"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // O ฝ่ายชนะ แนวทแยง 2
    @Test
    public void testCheckwinX2BY_O_output_true() {
        String[][] table = {{"-", "X", "O"}, {"X", "O", "-"}, {"O", "X", "-"}};
        String currentPlayer = "O";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // X ฝ่ายชนะ แนวทแยง 1
    @Test
    public void testCheckwinX1BY_X_output_true() {
        String[][] table = {{"X", "O", "O"}, {"-", "X", "-"}, {"-", "-", "X"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // X ฝ่ายชนะ แนวทแยง 2
    @Test
    public void testCheckwinX2BY_X_output_true() {
        String[][] table = {{"O", "-", "X"}, {"O", "X", "-"}, {"X", "-", "-"}};
        String currentPlayer = "X";
        assertEquals(true, OXProgram.checkWin(table, currentPlayer));
    }

    // เสมอ 1
    public void testCheckDraw1_output_true() {
        String[][] table = {{"O", "X", "O"}, {"O", "X", "O"}, {"X", "O", "X"}};
        String currentPlayer = "O";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
        assertEquals(true, OXProgram.checkDraw(table));
    }

    // เสมอ 2
    public void testCheckDraw2_output_true() {
        String[][] table = {{"X", "O", "X"}, {"X", "X", "O"}, {"O", "X", "O"}};
        String currentPlayer = "X";
        assertEquals(false, OXProgram.checkWin(table, currentPlayer));
        assertEquals(true, OXProgram.checkDraw(table));
    }
}
